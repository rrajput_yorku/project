const double = require('./double');

console.log(double(4));

function double(num) {
  return num * 2;
}

module.exports = double;
